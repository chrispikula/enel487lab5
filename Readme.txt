This is an application that can do the entirety of labs 1->4, as well as measure the height of a ball in a tube.

The data for the heights of the ball in the tube is in dataTable.pdf and data.ods

The chart produced is in data.pdf

The data was aquired by taking many repeated measurements of the ball at specific heights, while the device was on it's side.
Interesting to note is that the IR sensor seems to get 'stuck' at values near 511.