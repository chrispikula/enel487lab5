/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

Description: The device setup for USART setup
*/
#include "serialPortInit.h"

void USART2Open(void)
{
	RCC_APB2ENR |= (0x01 << 2); //enable the clocks for port A
	//first set up the port a registers	
	RCC_APB1ENR |= (0x01 << 17); //usart2 enabled
		
	GPIOA_ODR &= 0x00000000; //set the output bits to zero
	GPIOA_CRL &= 0xFFFF00FF;
	GPIOA_CRL |= 0x00008B00; //pg 166	0x44444444 is the reset value
													//we want PA2 to be A/F push-pull ->B
													//we want PA3 to be input floating/input pull-up -> 8
	//Now we want to set up the USART
	USART2_BRR |= 0x138; //F_CLK = 36MHz, 19.5 is our division value, gives a result
												//of 115.2, or more accutately 115.384kbps
	
	USART2_CR1 = ((uint16_t)0x2000) | ((uint16_t)0x0008) |  ((uint16_t)0x0004)  ;
	//USART2->CR2;
	//USART2->CR3;
}

void USART2Close(void)
{
	RCC_APB1ENR &= (0xFFFFFFFF - (0x01 << 17)); //Turn off the clock for usart2
	GPIOA_CRL &= 0xFFFF00FF; //Turn PA2/PA3 back to the default state.
	GPIOA_CRL |= 0xFFFF44FF;
	USART2_BRR = 0x00; //Turn the USART2->BRR back to the reset value
	USART2_CR1 = 0x00; //Turn the USART2->CR1 back to the reset value
}

uint8_t USART2SendString(const char* input)
{
	int i;
	uint8_t errorCheck = 0;
	for (i = 0; input[i] !='\0'; i++)
	{
		errorCheck = USART2SendByte(input[i]);
	}
	
		//errorCheck = USART2SendByte('\n');
		//errorCheck = USART2SendByte('\r');

	if (errorCheck == 1)
	{
		return 1;
	}
	else
	{
	return 0;
	}
}

uint8_t USART2SendByte(uint8_t Output)
{
	
	USART2_DR = Output;
	
	while((USART2_SR & ((uint16_t)0x0080)) == 0)
	{
	
	}
	return Output;
}

uint8_t USART2SendNumber(uint8_t input)
{
	uint8_t ones = input %10;
	uint8_t tens = input %100 / 10;
	uint8_t hundreds = input %1000 / 100;
	USART2SendByte(hundreds+48);
	USART2SendByte(tens+48);
	USART2SendByte(ones+48);
	return 0;
}

uint32_t USART2SendNumber32(uint32_t input)
{
	bool leadingZero = true;
	uint8_t ones = input %10;
	uint8_t tens = input %100 / 10;
	uint8_t hundreds = input 				%1000 				/ 100;
	uint8_t thousands = input 			%10000 				/ 1000;
	uint8_t tenthou = input 				%100000 			/ 10000;
	uint8_t hundthou = input 				%1000000 			/ 100000;
	uint8_t mil = input 						%10000000 		/ 1000000;
	uint8_t tenmil = input 					%100000000 		/ 10000000;
	uint8_t hundmil = input 				%1000000000 	/ 100000000;
	uint8_t bill = input 						%10000000000 	/ 1000000000;
	
	if (bill != 0)
	{
		USART2SendByte(bill+48);
		leadingZero = false;
	}
	if (hundmil != 0 || leadingZero == false)
	{
		USART2SendByte(hundmil+48);
		leadingZero = false;
	}
	if (tenmil != 0 || leadingZero == false)
	{
		USART2SendByte(tenmil+48);
		leadingZero = false;
	}
	if (mil != 0 || leadingZero == false)
	{
		USART2SendByte(mil+48);
		leadingZero = false;
	}
	if (hundthou != 0 || leadingZero == false)
	{
		USART2SendByte(hundthou+48);
		leadingZero = false;
	}
	if (tenthou != 0 || leadingZero == false)
	{
		USART2SendByte(tenthou+48);
		leadingZero = false;
	}
	if (thousands != 0 || leadingZero == false)
	{
		USART2SendByte(thousands+48);
		leadingZero = false;
	}
	if (hundreds != 0 || leadingZero == false)
	{
		USART2SendByte(hundreds+48);
		leadingZero = false;
	}
	if (tens != 0 || leadingZero == false)
	{
		USART2SendByte(tens+48);
		leadingZero = false;
	}					
	USART2SendByte(ones+48);
	return 0;
}



uint8_t USART2GetByte(uint8_t Input)
{
	while((USART2_SR & (uint16_t)0x0020) == 0 )
	{
	}
	
	return USART2_DR & 0x1FF;
}

