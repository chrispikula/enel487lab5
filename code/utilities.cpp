/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

Description: A set of utilities that are uses throughout the program.
*/

#include "utilities.h"
#include "time.h"
#include "stdlib.h"

//Our simpliest full-cpu usage delay function
void delay(uint32_t delay)
{
  int i=0;
	for(i=0; i< delay; ++i)
	{
	}	
}	

//A function that turns on a specific bit in the BSRR
void turnOnABitBSRR(volatile uint32_t * reg, uint8_t bit)
{
	* reg = (0x01 << (bit));
}

//A function that turns off a specific bit in the BSRR
void turnOffABitBSRR(volatile uint32_t * reg, uint8_t bit)
{
	* reg = (0x01 << (16+bit));
}

//Gives us a random integer number
uint32_t randomInt(void)
{
	return rand() %RAND_MAX;
}
