#include "IRSensor.h"

int ADC_CONVERSION_RESULT = 0;

void IRSensorInit(void)
{

	//enable the clocks for port C, and ADC1
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN | RCC_APB2ENR_ADC1EN; 
	
	//set PCO to input mode with a floating input.
	GPIOC->ODR &= 0x00000000; //set the output bits to zero
	GPIOC->CRL &= 0xFFFFFFF0;
	GPIOC->CRL |= 0x00000004; //GPIOC PIN 0 Set to floating Input
	
	//ADC1->CR2 |= 0x1; //Turn the ADC on
	
	//Setup so just one conversion happens.
	ADC1->SQR1 &= 0xFF000000;
	ADC1->SQR1 |= 0x0;//0x00100000; 
	
	//Channel 10 is our channel that will be processed
	ADC1->SQR3 &= 0xC0000000;
	ADC1->SQR3 |= 0x0000000A; 
	
	
	//Set the sample time for channel 10 to 55.5 cycles
	ADC1->SMPR1 &= 0xFFFFFFF8;
	ADC1->SMPR1 |= 0x5;
	
	ADC1->CR1 |= ADC_CR1_SCAN | ADC_CR1_EOCIE;
	ADC1->CR2 |= ADC_CR2_EXTTRIG | ADC_CR2_EXTSEL | ADC_CR2_ADON;
	
	//reset calibration
	ADC1->CR2 |= ADC_CR2_RSTCAL;
	while ((ADC1->CR2 & ADC_CR2_RSTCAL) == ADC_CR2_RSTCAL)
	{
	}
	
	//Do calibration
	ADC1->CR2 |= ADC_CR2_CAL;
	while ((ADC1->CR2 & ADC_CR2_CAL) == ADC_CR2_CAL)
	{
	}
	
	/*Move on to the control registers. Enable scan mode and interrupt enable for EOC. Enable
external triggering and use SWSTART as the external event select. Also turn the ADC on.
(ADC CR1 and ADC CR2)
	*/
	
		
}
void FindPositionSensor(void) //Can't be run in quick succession!
{
	ADC1->CR2 |= ADC_CR2_SWSTART;
}

int FindPosition(void)
{
	while(ADC1->SR &= 0x1F)
	{
		
	}
	int height = ADC_CONVERSION_RESULT;
	int result = 0;
	
	if (height < 500)
	{
		result = 61;
	}
	else if (height < 511)
	{
		result = LinearInterpolation (500, 61, 511, 56, height);
	}
	else if (height < 566)
	{
		result = LinearInterpolation (511, 56, 566, 51, height);
	}
	else if (height < 615)
	{
		result = LinearInterpolation (566, 51, 615, 46, height);
	}
	else if (height < 686)
	{
		result = LinearInterpolation (615, 46, 686, 41, height);
	}
	else if (height < 801)
	{
		result = LinearInterpolation (686, 41, 801, 36, height);
	}
	else if (height < 982)
	{
		result = LinearInterpolation (801, 36, 982, 31, height);
	}
	else if (height < 1287)
	{
		result = LinearInterpolation (982, 31, 1287, 26, height);
	}
	else if (height < 1789)
	{
		result = LinearInterpolation (1287, 26, 1789, 21, height);
	}
	else if (height < 2334)
	{
		result = LinearInterpolation (1789, 21, 2334, 16, height);
	}
	else if (height < 2728)
	{
		result = LinearInterpolation (2334, 16, 2728, 11, height);
	}
	else if (height < 2748)
	{
		result = 10;
	}
	else if (height < 2838)
	{
		result = 9;
	}
	else if (height < 3056)
	{
		result = 8;
	}
	else if (height < 3340)
	{
		result = 7;
	}
	else if (height < 3675)
	{
		result = 6;
	}
	else if (height < 3796)
	{
		result = 5;
	}
	else
	{
		result = 4;
	}
	//fudge factor
	//result = result * 3 / 2;
	return result;
}

int LinearInterpolation (int x1, int y1, int x3, int y3, int x2)
{
	int y2 = y3 + (x2 - x1)*(y3 - y1) / (x3 - x1);
	return y2;
}

void InitializeHeight(void)
{
	//set PWM to 100
}
