/**
Programmer: Chris Pikula
Project: ServoMotor
Date: 2016-10-24

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)
It also initializes timer 4, and can run pwm on channel 2.

This file is that initializes and closes the timers
*/


#include "timer_impl.h"
#include "stm32f10x.h"
#include "registers.h"
#include "math.h"

//Initialize timer 3 with interrupts
void timer_init_with_interrupts(void)
{
		RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	
	//OPen port A for our LEDS;
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	GPIOA->ODR &= 0x00;
	GPIOA->CRL &= 0xFFFF00FF;
	GPIOA->CRL |= 0x00008B00;
	
	TIM3->PSC = 7199;
	TIM3->ARR = 4999;

	
	TIM3->DIER = TIM_DIER_UIE;
	
	//Interrupt Won't Work.
	
	
	TIM3->CR1 = TIM_CR1_CEN;
}

//Initializie timer 2 without interrupts
void TimerInit(void)
{
	//Enable the Timer for our clock
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	
	
	TIM2->PSC = 0;
	TIM2->ARR = 0xFFFF;
	
	TIM2->CR1 = TIM_CR1_DIR | TIM_CR1_CEN;
}

//return the value of timer 2
int16_t timer_start(void)
{
	return TIM2->CNT;
}

//return the difference of two values of timer 2
int16_t timer_stop(int16_t start_time)
{
	int16_t temp = TIM2->CNT;
	if (temp > start_time)
	{
		return (0xFFFF - start_time + temp) % 0xFFFF;
	}
	else
	{
		return start_time - temp;
	}
}

//Shut down timer 2
void timer_shutdown()
{
	TIM2->CR1 = 0;
	RCC->APB1ENR &= (0xFFFFFFFF - RCC_APB1ENR_TIM2EN);
}

//Shut down timer 3
void timer3_shutdown()
{
	TIM3->CR1 = 0;
	RCC->APB1ENR &= (0xFFFFFFFF - RCC_APB1ENR_TIM3EN);
}
void timer4Init(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
	
  TIM4->CR1 = 0;		// reset command register 1
  TIM4->CR2 = 0;		// reset command register 2
	
	TIM4->PSC = 35; //72/8-1 = 8
	TIM4->ARR = 39999; //20000*8-1
	
	TIM4->CCR1 = 0;				//__TIM4_CCR1;  //0
  TIM4->CCR2  = 3000;      //__TIM4_CCR2;  //0
  //TIM4->CCR3  = 240;//0x1388; //__TIM4_CCR3;  //0x1388 = 2500
  //TIM4->CCR4  = 600;//0x09c4; //__TIM4_CCR4;  //0x09c4 = 1250
  TIM4->CCMR1 =	0x6000;      //__TIM4_CCMR1; //0x6000
  //TIM4->CCMR2 = 0x6060; //__TIM4_CCMR2; //0x6060
  //TIM4->CCER  |= 0x1110; //__TIM4_CCER;  //0x1110 set capture/compare enable register
	TIM4->CCER |= 0x0010;
  TIM4->SMCR  = 0;      //__TIM4_SMCR;    // 0 set slave mode control register

  TIM4->CR1 = 1u<<2;    // 1: URS: Only counter overflow/underflow
			  // generates an update interrupt or DMA
			  // request if enabled.

  TIM4->CR2 = 0;        //__TIM4_CR2; // 0x0 set command register 2


  TIM4->CR1 |= 1u<<0;  // 0: enable timer

	
	//RCC_APB1ENR
}
void timer4shutdown()
{
	TIM4->CR1 &= 0xFFFFFFFE;
	RCC->APB1ENR &= 0xFFFFFFFF - RCC_APB1ENR_TIM4EN;
}
